import json
import matplotlib.pyplot as plt
from pprint import pprint as pp

with open('sample.json') as json_file:
    json_data = json.load(json_file)

# pp(json_data["data"])
# print len(json_data["data"])

strokes = [];
for i in range(0, len(json_data["data"])):
    strokes.append(json_data["data"][i])

print "Number of strokes: {}".format(len(strokes))

# pp(strokes[0][0]["x"])

first_stroke = strokes[2]

print "Calculating the min & max values..."

max_x = 0.0;
min_x = 100000.0;
max_y = 0.0;
min_y = 100000.0
for point in first_stroke:
    if point["x"] > max_x:
        max_x = point["x"]
    if point["x"] < min_x:
        min_x = point["x"]
    if point["y"] > max_y:
        max_y = point["y"]
    if point["y"] < min_y:
        min_y = point["y"]

print "MAX X: {}".format(max_x)
print "MIN X: {}".format(min_x)
print "MAX Y: {}".format(max_y)
print "MIN Y: {}".format(min_y)

x_coords = []
y_coords = []
for point in first_stroke:
    x_coords.append(point["x"] - min_x)
    y_coords.append(point["y"] - min_y)

plt.scatter(x_coords, y_coords)
plt.grid()
plt.show()
