## Instructions:

#### Requirements:
* **OSX/Linux** - TensorFlow doesn't have Windows destribution. (for the notebooks that require TensorFlow)


#### 1. Install pip and Virtualenv:
```bash
    # Ubuntu/Linux 64-bit
    $ sudo apt-get install python-pip python-dev python-virtualenv

    # Mac OS X
    $ sudo easy_install pip
    $ sudo pip install --upgrade virtualenv
```

#### 2. Create a Virtualenv environment in the directory ~/tensorflow:
```bash
    $ virtualenv --system-site-packages ~/tensorflow
```

#### 3. Activate the environment:
```bash
    $ source ~/tensorflow/bin/activate  # If using bash
    $ source ~/tensorflow/bin/activate.csh  # If using csh
    (tensorflow)$  # Your prompt should change
```

#### 4. Install project dependencies:
```bash
    # Python 2
    (tensorflow)$ pip install -r requirements.txt
    
    # Python 3
    (tensorflow)$ pip3 install -r requirements.txt
```

#### 5. Select the TensorFlow binary to install:
```bash
    # Ubuntu/Linux 64-bit, CPU only, Python 2.7
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.11.0rc0-cp27-none-linux_x86_64.whl
    
    # Ubuntu/Linux 64-bit, GPU enabled, Python 2.7
    # Requires CUDA toolkit 7.5 and CuDNN v5. For other versions, see "Install from sources" below.
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-0.11.0rc0-cp27-none-linux_x86_64.whl
    
    # Mac OS X, CPU only, Python 2.7:
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-0.11.0rc0-py2-none-any.whl
    
    # Mac OS X, GPU enabled, Python 2.7:
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/mac/gpu/tensorflow-0.11.0rc0-py2-none-any.whl
    
    # Ubuntu/Linux 64-bit, CPU only, Python 3.4
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.11.0rc0-cp34-cp34m-linux_x86_64.whl
    
    # Ubuntu/Linux 64-bit, GPU enabled, Python 3.4
    # Requires CUDA toolkit 7.5 and CuDNN v5. For other versions, see "Install from sources" below.
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-0.11.0rc0-cp34-cp34m-linux_x86_64.whl
    
    # Ubuntu/Linux 64-bit, CPU only, Python 3.5
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.11.0rc0-cp35-cp35m-linux_x86_64.whl
    
    # Ubuntu/Linux 64-bit, GPU enabled, Python 3.5
    # Requires CUDA toolkit 7.5 and CuDNN v5. For other versions, see "Install from sources" below.
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/gpu/tensorflow-0.11.0rc0-cp35-cp35m-linux_x86_64.whl
    
    # Mac OS X, CPU only, Python 3.4 or 3.5:
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-0.11.0rc0-py3-none-any.whl
    
    # Mac OS X, GPU enabled, Python 3.4 or 3.5:
    (tensorflow)$ export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/mac/gpu/tensorflow-0.11.0rc0-py3-none-any.whl
```

#### 6. Install TensorFlow:
```bash
    # Python 2
    (tensorflow)$ pip install --upgrade $TF_BINARY_URL
    
    # Python 3
    (tensorflow)$ pip3 install --upgrade $TF_BINARY_URL
```

Separate TensorFlow instructions can be found [here](https://www.tensorflow.org/versions/r0.11/get_started/os_setup.html#virtualenv-installation).

#### 7. Run jupyter notebooks:
Navigate to the notebook directory and type:
```bash
    (tensorflow).../clock/notebook$ jupyter notebook
```

**The file "requirements.txt" has all required dependencies for the virtualenv.**

### How to use "requirements.txt (alternative)":
```bash
    $ virtualenv --no-site-packages --distribute .env && source .env/bin/activate && pip install -r requirements.txt
```